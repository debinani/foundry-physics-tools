# Traveller Physics Tools for FoundryVTT

Some chat tools for FoundryVTT, which provide some useful physics tools
for use in Traveller and other SciFi RPGs.

Ported from my Roll20 scripts.

You can install this in Foundry, either by copying in the zip file from
the releases directory, and unzipping it in Data/modules, or by going to
'Install Module' in Foundry, and using the following Manifest URL:

```
https://gitlab.com/samuelpenn/foundry-physics-tools/-/raw/main/releases/module.json
```

## Usage

Type `/physics` into the chat to get a full list of the commands.

To get information about a planet, type the following into the chat:

```
/phy planet 4500km 5
```

It will report information on surface gravity, escape velocity and mass
for a planet of radius 4,500km and density 5.

```
/phy planet 4500km 5 +500km
```

This will add information on an orbit at 500km altitude above that planet.

### Units

Distance units default to kilometres unless otherwise specified. Density is
assumed to be in g/cm³. Other standard units that can be specified include:

  * m - Metres (distance)
  * km - Kilometres (distance)
  * Mkm - Millions of kilometres (distance)
  * AU - Astronomical Units (distance)
  * lm - Light Minutes (distance)
  * lh - Light Hours (distance)
  * ld - Light Days (distance)
  * lw - Light Weeks (distance)
  * ly - Light Years (distance)
  * pc - Parsecs (distance)

Units are case insensitive. Other units that can be used are:

  * e - Either radius of Earth, or density of Earth.
  * m - Density of the Moon (density only)
  * j - Radius of Jupiter or density of Jupiter
  * sol - Radius of Sol, or density of Sol.

![Examples](docs/screenshot-1.jpg)


### Planet Commands

The planet commands are designed to provide information about a planet.
Given the planet's radius and density, it will output:

  * Mass
  * Escape Velocity
  * Surface Gravity

Density is specified in g/cm³, which is 5.5 for Earth, 3.4 for the Moon,
1.33 for Jupiter and 1.4 for the sun. You can use these planets as 'units'
for both distances (radius) and density.

For example:

```
/physics planet 1.2e 1e
/physics planet 1000 3.5
```

Are both ways of getting information about a planet.

You can also get information about orbits around planets. For example:

```
/physics planet 1e 1e +400km
/physics planet 1e 1e 380000km
```

These will return information on the orbital velocity and orbital period
of those orbits, as well as escape velocity and gravity at that orbit.

If you specify a + for the orbit, it is taken as an altitude, otherwise
it is taken as the distance from the centre of the planet.

### Travel Times

You can also work out travel times, assuming accelerating to the half way
point and then decellerating.

```
/physics thrust 1g 5au
```

This will tell you how long it will take to travel 5au whilst accelerating
at 1g. It will also tell you maximum velocity achieved. In case you want to
ram your target, it will also give you impact time and velocity without
slowing down.

For a bit more accuracy at high velocities, you can use `ethrust` instead:

```
/physics ethrust 10g 1ly
```

This will give you both actual time to impact, and ship time, accounting for
reletavistic time dilation.

### Other Commands

Finally, there are commands that don't fit nicely into the above categories.

Finally, you can work out the classic rocket equation, by providing the wet
mass of the ship (fully fueled), the dry mass (without fuel) and the specific
impulse (in seconds).

```
/physics rocket 1000 200 350
```

This allows you to work out the classic rocket equation. If you provide the
wet mass (fully fueled) of your ship, the dry mass (without fuel) and the
specific impulse (in seconds), it will provide you with your Delta Vee.

Not often needed in Traveller, but it's there in case it's useful.

```
/physics lag 5au
```

Calculates the light lag given a distance. Returns the time taken for light
to travel that distance, plus also the time for light to bounce back (simply
twice the first value). Can be used to work out the delay in radio
communication, or lag in radar signals.