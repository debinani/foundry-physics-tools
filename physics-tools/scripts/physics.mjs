// noinspection JSUnresolvedVariable,JSUnresolvedFunction

export const Physics = {};

Physics.LIGHTYEAR = 9.461e15;
Physics.AU = 149597870700;
Physics.G = 6.6743e-11;
Physics.C = 299792458;
Physics.g = 9.807;

Physics.EARTH_RADIUS = 6371000;
Physics.EARTH_DENSITY = 5.51;
Physics.EARTH_MASS = 5.972e24;
Physics.JUPITER_MASS = 1.898e27;
Physics.SOL_MASS = 1.989e30;
Physics.SOL_RADIUS = 696340000;
Physics.MERCURY_DENSITY = 5.43;
Physics.VENUS_DENSITY = 5.24;
Physics.MARS_DENSITY = 3.93;
Physics.MOON_DENSITY = 3.34;
Physics.JUPITER_DENSITY = 1.33;
Physics.SATURN_DENSITY = 0.687;
Physics.URANUS_DENSITY = 1.27;
Physics.NEPTUNE_DENSITY = 1.64;
Physics.PLUTO_DENSITY = 1.88;
Physics.MERCURY_RADIUS = 2439700;
Physics.VENUS_RADIUS = 6051800;
Physics.MARS_RADIUS = 3389500;
Physics.JUPITER_RADIUS = 69911000;
Physics.SATURN_RADIUS = 58232000;
Physics.URANUS_RADIUS = 25362000;
Physics.NEPTUNE_RADIUS = 24622000;
Physics.PLUTO_RADIUS = 1188300;
Physics.MOON_RADIUS = 1737400;
Physics.SOL_DENSITY = 1.41;
Physics.STANDARD_DAY = 86400;
Physics.STANDARD_YEAR = Physics.STANDARD_DAY * 365;


Physics.chatMessage = function(text) {
    let whisper = game.settings.get('physics-tools', 'whisper')?[ game.userId ]: [ ];
    let chatData = {
        user: game.userId,
        speaker: ChatMessage.getSpeaker(),
        content: text,
        whisper: whisper
    }
    ChatMessage.create(chatData, {});
};

Physics.getNumber = function(number) {
    if (!number || number === "") {
        return 0;
    }
    return parseFloat(number.replace(/[^0-9.\-]/g, ""));
};

Physics.getThrust = function (thrust) {
    if (!thrust || thrust === "") {
        return Physics.g;
    }
    if ((""+thrust).match(/^[0-9.\-]+$/)) {
        return parseFloat(thrust);
    }
    thrust = ("" + thrust).toLowerCase();
    let number = parseFloat(thrust.replace(/[^0-9.\-]/g, ""));

    if (thrust.match("g$")) {
        number *= Physics.g;
    }

    return parseFloat(number);
};

// Returns a float, always in metres. Assumes given in km unless otherwise specified
Physics.getDistance = function (distance) {
    let diameter = 1;
    if (game.settings.get("physics-tools", "useDiameter")) {
        diameter = 2;
    }
    if (!distance || distance === "") {
        return Physics.EARTH_RADIUS * diameter;
    }
    if ((""+distance).match(/^[0-9.\-]+$/)) {
        return parseFloat(distance) * 1000;
    }
    distance = ("" + distance).toLowerCase();
    let number = parseFloat(distance.replace(/[^0-9.\-]/g, ""));

    if (distance.match("mkm$")) {
        number *= 1000000000;
    } else if (distance.match("lm$")) {
        number *= Physics.C * 60;
    } else if (distance.match("lh$")) {
        number *= Physics.C * 3600;
    } else if (distance.match("ld$")) {
        number *= Physics.C * 86400;
    } else if (distance.match("lw$")) {
        number *= Physics.C * 86400 * 7;
    } else if (distance.match("km$")) {
        number *= 1000;
    } else if (distance.match("m$")) {
        number *= 1;
    } else if (distance.match("au$")) {
        number *= Physics.AU;
    } else if (distance.match("ly$")) {
        number *= Physics.C * 86400 * 365.25;
    } else if (distance.match("pc$")) {
        number *= Physics.C * 86400 * 365.25 * 3.2616;
    } else if (distance.match("mercury$")) {
        number *= Physics.MERCURY_RADIUS * diameter;
    } else if (distance.match("venus$")) {
        number *= Physics.VENUS_RADIUS * diameter;
    } else if (distance.match("mars$")) {
        number *= Physics.MARS_RADIUS * diameter;
    } else if (distance.match("jupiter$") || distance.match("j$")) {
        number *= Physics.JUPITER_RADIUS * diameter;
    } else if (distance.match("saturn$")) {
        number *= Physics.SATURN_RADIUS * diameter;
    } else if (distance.match("uranus$")) {
        number *= Physics.URANUS_RADIUS * diameter;
    } else if (distance.match("neptune$")) {
        number *= Physics.NEPTUNE_RADIUS * diameter;
    } else if (distance.match("pluto$")) {
        number *= Physics.PLUTO_RADIUS * diameter;
    } else if (distance.match("earth$") || distance.match("e$")) {
        number *= Physics.EARTH_RADIUS * diameter;
    } else if (distance.match("moon$")) {
        number *= Physics.MOON_RADIUS * diameter;
    } else if (distance.match("sun$")) {
        number *= Physics.SOL_RADIUS * diameter;
    } else if (distance.match("sol$")) {
        number *= Physics.SOL_RADIUS * diameter;
    }

    return parseFloat(number);
};

// Returns a float, always in metres.
Physics.getDensity = function (density) {
    if (!density || density === "") {
        return Physics.EARTH_DENSITY;
    }
    if ((""+density).match(/^[0-9.\-]+$/)) {
        return parseFloat(density);
    }
    density = ("" + density).toLowerCase();
    let number = parseFloat(density.replace(/[^0-9.\-]/g, ""));

    if (density.match("e$") || density.match("earth$")) {
        number *= Physics.EARTH_DENSITY;
    } else if (density.match("mercury$")) {
        number *= Physics.MERCURY_DENSITY;
    } else if (density.match("venus")) {
        number *= Physics.VENUS_DENSITY;
    } else if (density.match("mars$")) {
        number *= Physics.MARS_DENSITY;
    } else if (density.match("jupiter$") || density.match("j$")) {
        number *= Physics.JUPITER_DENSITY;
    } else if (density.match("saturn$")) {
        number *= Physics.SATURN_DENSITY;
    } else if (density.match("uranus$")) {
        number *= Physics.URANUS_DENSITY;
    } else if (density.match("neptune$")) {
        number *= Physics.NEPTUNE_DENSITY;
    } else if (density.match("pluto$")) {
        number *= Physics.PLUTO_DENSITY;
    } else if (density.match("moon$")) {
        number *= Physics.MOON_DENSITY;
    } else if (density.match("sol$") || sensity.match("sun$")) {
        number *= Physics.SOL_DENSITY;
    }

    return parseFloat(number);
};

Physics.printNumber = function (number, precision) {
    number = parseFloat(number);
    if (precision === null || precision === undefined || precision < 0) {
        precision = 2;
    }

    if (number > 1e12 || number < 1e-3) {
        return number.toExponential(precision);
    } else if (number > 99) {
        return Number(parseInt(number)).toLocaleString();
    } else {
        return number.toPrecision(precision);
  }
};

// Takes time in seconds.
Physics.printTime = function (number) {
    let time = "";
    let count = 0;

    if (number > Physics.STANDARD_YEAR * 100) {
        count++;
    }

    if (number > Physics.STANDARD_YEAR * 10) {
        count++;
    }

    if (number >= Physics.STANDARD_YEAR) {
        let years = parseInt (number / Physics.STANDARD_YEAR);
        if (years > 100) {
        time += Physics.printNumber(years) + "y ";
        } else {
        time += years + "y ";
        }
        number %= Physics.STANDARD_YEAR;
        count++;
    }
    if (count > 0 || number >= Physics.STANDARD_DAY) {
        let days = parseInt(number / Physics.STANDARD_DAY);
        if (days > 100) {
        time += Physics.printNumber(days) + "d ";
        } else {
        time += days + "d ";
        }
        number %= Physics.STANDARD_DAY;
        count++;
    }
    if (count > 2) return time;

    if (count > 0 || number >= 3600) {
        time += parseInt(number / 3600) + "h ";
        number %= 3600;
        count++;
    }
    if (count > 2) return time;

    if (count > 0 || number >= 60) {
        time += parseInt( number / 60) + "m ";
        number %= 60;
        count++;
    }
    if (count > 2) return time;

    if (time === "") {
        time += Physics.printNumber(number) + "s";
    } else {
        time += parseInt(number) + "s";
    }

    return time;
};

// Takes a distance in metres.
Physics.printDistance = function (number) {
    number = parseInt(number);

    let units = "m";
    if (number > Physics.AU * 50000) {
        units = "LY";
        number = (1.0 * number) / Physics.LIGHTYEAR;
    } else if (number > Physics.AU * 2) {
        units = "AU";
        number = (1.0 * number) / Physics.AU;
    } else if (number > 2_000_000_000) {
        units = "Mkm";
        number = (1.0 * number) / 1_000_000_000;
    } else if (number >= 10_000) {
        units = "km";
        number = number / 1_000;
    }

    return Physics.printNumber(number) + units;
};

Physics.printVelocity = function(title, velocity) {
    let text = "";

    text += `<b>${title}</b>: ${Physics.printNumber(velocity / 1000)}km/s`;
    if (velocity >= Physics.C) {
        text += " <i>(!)</i><br/>";
    } else if (velocity > Physics.C / 10) {
        text += ` (${(velocity / Physics.C).toFixed(2)}c)<br/>`;
        let td = Math.sqrt( 1 - (velocity * velocity) / (Physics.C * Physics.C));
        text += `<b>Time dilation</b>: ${td.toFixed(3)}<br/>`;
    } else {
        text += "<br/>";
    }

    return text;
};

Physics.help = function(chatData) {
    let text = `<div class="physics-tools">`;
    text += `<h2>${game.i18n.localize("PhysicsTools.Title")}</h2>`;

    text += `/physics &lt;command> &lt;args><br/><br/>`;

    text += `<h3>${game.i18n.localize("PhysicsTools.Commands")}</h3>`;

    text += `planet [${game.i18n.localize("PhysicsTools.Radius")}] [${game.i18n.localize("PhysicsTools.Density")}]<br/>`;
    text += `planet [${game.i18n.localize("PhysicsTools.Radius")}] [${game.i18n.localize("PhysicsTools.Density")}] [${game.i18n.localize("PhysicsTools.OrbitDistance")}]<br/>`;
    text += `thrust [g] [${game.i18n.localize("PhysicsTools.Distance")}]<br/>`;
    text += `ethrust [g] [${game.i18n.localize("PhysicsTools.Distance")}]<br/>`;
    text += `rocket [${game.i18n.localize("PhysicsTools.WetMassArg")}] [${game.i18n.localize("PhysicsTools.DryMassArg")}] [isp]<br/>`;
    text += `lag [${game.i18n.localize("PhysicsTools.Distance")}]<br/><br/>`;

    text += `<h3>${game.i18n.localize("PhysicsTools.Units")}</h3>`;

    text += `m - ${game.i18n.localize("PhysicsTools.m")}<br/>`;
    text += `km - ${game.i18n.localize("PhysicsTools.km")}<br/>`;
    text += `Mkm - ${game.i18n.localize("PhysicsTools.mkm")}<br/>`;
    text += `AU - ${game.i18n.localize("PhysicsTools.au")}<br/>`;
    text += `lm - ${game.i18n.localize("PhysicsTools.lm")}<br/>`;
    text += `lh - ${game.i18n.localize("PhysicsTools.lh")}<br/>`;
    text += `ld - ${game.i18n.localize("PhysicsTools.ld")}<br/>`;
    text += `lw - ${game.i18n.localize("PhysicsTools.lw")}<br/>`;
    text += `ly - ${game.i18n.localize("PhysicsTools.ly")}<br/>`;
    text += `pc - ${game.i18n.localize("PhysicsTools.pc")}<br/>`;

    text += "<br/>";
    text += `<h3>${game.i18n.localize("PhysicsTools.CommonUnits")}</h3>`

    text += `Earth|E - ${game.i18n.localize("PhysicsTools.Earth")}`;
    text += ` <span class="values">(6,371km, 5.51g/cm³)</span><br/>`;
    text += `Jupiter|J - ${game.i18n.localize("PhysicsTools.Jupiter")}`;
    text += ` <span class="values">(69,911km, 1.33g/cm³)</span><br/>`;
    text += `Sol|Sun - ${game.i18n.localize("PhysicsTools.Sol")}`;
    text += ` <span class="values">(696,340km, 1.41g/cm³)</span><br/>`;
    text += `Mercury - ${game.i18n.localize("PhysicsTools.Mercury")}`;
    text += ` <span class="values">(2,440km, 5.43g/cm³)</span><br/>`;
    text += `Venus - ${game.i18n.localize("PhysicsTools.Venus")}`;
    text += ` <span class="values">(6,052km, 5.24g/cm³)</span><br/>`;
    text += `Moon - ${game.i18n.localize("PhysicsTools.Moon")}`;
    text += ` <span class="values">(1,737km, 3.34g/cm³)</span><br/>`;
    text += `Mars - ${game.i18n.localize("PhysicsTools.Mars")}`;
    text += ` <span class="values">(3,390km, 3.93g/cm³)</span><br/>`;
    text += `Saturn - ${game.i18n.localize("PhysicsTools.Saturn")}`;
    text += ` <span class="values">(58,232km, 0.687g/cm³)</span><br/>`;
    text += `Uranus - ${game.i18n.localize("PhysicsTools.Uranus")}`;
    text += ` <span class="values">(25,362km, 1.27g/cm³)</span><br/>`;
    text += `Neptune - ${game.i18n.localize("PhysicsTools.Neptune")}`;
    text += ` <span class="values">(24,622km, 1.64g/cm³)</span><br/>`;
    text += `Pluto - ${game.i18n.localize("PhysicsTools.Pluto")}`;
    text += ` <span class="values">(1,188km, 1.88g/cm³)</span><br/>`;
    text += `</div>`;

    Physics.chatMessage(text);
};

Physics.planetCommand = function(chatData, args) {
    let text = `<div class="physics-tools">`;
    text += `<h3>${game.i18n.localize("PhysicsTools.PlanetData")}</h3>`;

    if (args.length < 2) {
        return;
    }

    let radius = Physics.getDistance(args.shift());
    if (game.settings.get("physics-tools", "useDiameter")) {
        radius /= 2;
    }
    let density = Physics.getDensity(args.shift());

    let mass = 4.0/3.0 * Math.PI * radius * radius * radius * density * 1000;
    let g = mass * Physics.G / (radius * radius);
    let ev = Math.sqrt(mass * Physics.G * 2 / radius);

    text += `<b>${game.i18n.localize("PhysicsTools.Radius")}</b>: ${Physics.printNumber(radius / 1000)}km<br/>`;
    text += `<b>${game.i18n.localize("PhysicsTools.Density")}</b>: ${Physics.printNumber(density)}g/cm³<br/>`;
    text += `<b>${game.i18n.localize("PhysicsTools.Mass")}</b>: ${Physics.printNumber(mass)}kg<br/>`;
    if (mass > Physics.SOL_MASS / 10) {
        text += `&nbsp;<i><b>${game.i18n.localize("PhysicsTools.Mass")}</b>: ${Physics.printNumber(mass / Physics.SOL_MASS)} Sols</i><br/>`;
    }
    if (mass > Physics.JUPITER_MASS / 10 && mass < Physics.JUPITER_MASS * 200) {
        text += `&nbsp;<i><b>${game.i18n.localize("PhysicsTools.Mass")}</b>: ${Physics.printNumber(mass / Physics.JUPITER_MASS)} Jupiters</i><br/>`;
    }
    if (mass > Physics.EARTH_MASS / 100 && mass < Physics.EARTH_MASS * 200) {
        text += `&nbsp;<i><b>${game.i18n.localize("PhysicsTools.Mass")}</b>: ${Physics.printNumber(mass / Physics.EARTH_MASS)} Earths</i><br/>`;
    }
    // Escape velocity
    if (ev >= Physics.C) {
        text += `<i><b>${game.i18n.localize("PhysicsTools.EscapeVelocity")}</b>: ${game.i18n.localize("PhysicsTools.NoEscape")}.</i><br/>`;
    } else {
        text += `<b>${game.i18n.localize("PhysicsTools.EscapeVelocity")}</b>: ${Physics.printNumber(ev)}m/s<br/>`;
        if (ev > Physics.C / 100) {
        text += `&nbsp;<i><b>${game.i18n.localize("PhysicsTools.EscapeVelocity")}</b>: ${Physics.printNumber(ev / Physics.C)}c</i><br/>`;
        }
        text += `<b>${game.i18n.localize("PhysicsTools.SurfaceGravity")}</b>: ${Physics.printNumber(g)}m/s²<br/>`;
        if (g > 0.1) {
        text += `&nbsp;<i><b>${game.i18n.localize("PhysicsTools.SurfaceGravity")}</b>: ${Physics.printNumber(g / Physics.g)}g</i><br/>`;
        }
    }

    if (args.length > 0) {
        let value = args.shift();
        let orbit = Physics.getDistance(value);
        if (value.startsWith("+")) {
            orbit += radius;
        }
        let velocity = Math.sqrt(Physics.G * mass / orbit);
        let circumference = 2 * Math.PI * orbit;
        let time = circumference / velocity;
        let evo = Math.sqrt(mass * Physics.G * 2 / orbit);
        let og = mass * Physics.G / (orbit * orbit);

        let orbitDistance = Physics.printNumber(orbit / 1000) + "km";
        if (orbit > Physics.AU * 2) {
            orbitDistance = Physics.printNumber(orbit / Physics.AU) + "AU";
        } else if (orbit > 10000000000) {
            orbitDistance = Physics.printNumber(orbit / 1000000000) + "Mkm";
        } else if (orbit > 100000000) {
            orbitDistance = Physics.printNumber(orbit / 1000000) + "Kkm";
        }

        text += `<br/><h3>${orbitDistance} ${game.i18n.localize("PhysicsTools.orbit")}</h3>`;

        if (evo >= Physics.C) {
        text += `<i>${game.i18n.localize("PhysicsTools.NoOrbitPossible")}</i><br/>`;
        } else {
        text += `<b>${game.i18n.localize("PhysicsTools.OrbitVelocity")}</b>: ${Physics.printNumber(velocity)}m/s<br/>`;
        text += `<b>${game.i18n.localize("PhysicsTools.OrbitPeriod")}</b>: ${Physics.printTime(time)}<br/>`;
        text += `<b>${game.i18n.localize("PhysicsTools.EscapeVelocity")}</b>: ${Physics.printNumber(evo)}m/s<br/>`;
        if (evo > Physics.C / 100) {
            text += `&nbsp;<i><b>${game.i18n.localize("PhysicsTools.EscapeVelocity")}</b>: ${Physics.printNumber(evo / Physics.C)}c</i><br/>`;
        }
        text += `<b>${game.i18n.localize("PhysicsTools.Gravity")}</b>: ${Physics.printNumber(og)}m/s²<br/>`;
        if (og > 0.1) {
            text += `&nbsp;<i><b>${game.i18n.localize("PhysicsTools.Gravity")}</b>: ${Physics.printNumber(og / Physics.g)}g</i><br/>`;
        }
        }
    }
    text += `</div>`;

    Physics.chatMessage(text);

};

Physics.thrustCommand = function(chatData, args) {
    let text = `<div class="physics-tools">`;
    text += `<h3>${game.i18n.localize("PhysicsTools.TravelTimes")}</h3>`;

    if (args.length < 2) {
        return;
    }

    let thrust = Physics.getThrust(args.shift());
    let distance = Physics.getDistance(args.shift());

    let time = parseInt(2 * Math.sqrt(distance / thrust ));
    let maxv = thrust * time / 2;

    text += `<b>${game.i18n.localize("PhysicsTools.Thrust")}</b>: ${Physics.printNumber(thrust)}m/s² (${Physics.printNumber(thrust / Physics.g)}g) <br/>`;
    text += `<b>${game.i18n.localize("PhysicsTools.Distance")}</b>: ${Physics.printDistance(distance)}<br/>`;

    text += `<b>${game.i18n.localize("PhysicsTools.Time")}</b>: ${Physics.printTime(time)}<br/>`;
    text += Physics.printVelocity(game.i18n.localize("PhysicsTools.MaxVelocity"), maxv);

    // But what if we don't want to stop, and just thrust until impact?
    time = parseInt(Math.sqrt(2 * distance / thrust));
    maxv = thrust * time;

    text += "<br/>";
    text += `<b>${game.i18n.localize("PhysicsTools.ImpactTime")}</b>: ${Physics.printTime(time)}<br/>`;
    text += Physics.printVelocity(game.i18n.localize("PhysicsTools.ImpactVelocity"), maxv);
    text += `</div>`;

    Physics.chatMessage(text);
};

Physics.eThrustCommand = function(chatData, args) {
    let text = `<div class="physics-tools">`;
    text += `<h3>${game.i18n.localize("PhysicsTools.TravelTimes")}</h3>`;

    if (args.length < 2) {
        return;
    }

    let thrust = Physics.getThrust(args.shift());
    let distance = Physics.getDistance(args.shift());

    let f = Math.acosh( 1 + (thrust / 2) * distance / Math.pow(Physics.C,2 ));
    // Passage of time as perceived by the ship
    let shipTime = parseInt ( 2 * ( Physics.C / thrust) * f);
    // Passage of time as perceived by the outside universe
    let restTime = parseInt(2 * ( Physics.C / thrust) * Math.sinh(f));

    text += `<b>${game.i18n.localize("PhysicsTools.Thrust")}</b>: ${Physics.printNumber(thrust)}m/s² (${Physics.printNumber(thrust / Physics.g)}g) <br/>`;
    text += `<b>${game.i18n.localize("PhysicsTools.Distance")}</b>: ${Physics.printDistance(distance)}<br/>`;

    text += `<b>${game.i18n.localize("PhysicsTools.Time")}</b>: ${Physics.printTime(restTime)}<br/>`;
    if (restTime * 0.999 > shipTime) {
        text += `<b>${game.i18n.localize("PhysicsTools.ShipTime")}</b>: ${Physics.printTime(shipTime)}<br/>`;
    }

    f = Math.acosh( 1 + (thrust) * distance / Math.pow(Physics.C,2 ));
    // Passage of time as perceived by the ship
    shipTime = parseInt ( ( Physics.C / thrust) * f);
    // Passage of time as perceived by the outside universe
    restTime = parseInt(( Physics.C / thrust) * Math.sinh(f));
    text += "<br/>";
    text += `<b>${game.i18n.localize("PhysicsTools.ImpactTime")}</b>: ${Physics.printTime(restTime)}<br/>`;
    if (restTime * 0.999 > shipTime) {
        text += `<b>${game.i18n.localize("PhysicsTools.ShipImpactTime")}</b>: ${Physics.printTime(shipTime)}<br/>`;
    }
    text += `</div>`;

    Physics.chatMessage(text);
};

Physics.rocketCommand = function(chatData, args) {
    let text = `<div class="physics-tools">`;
    text += `<h3>${game.i18n.localize("PhysicsTools.RocketEquation")}</h3>`;

    if (args.length < 3) {
        return;
    }
    let wet = Physics.getNumber(args.shift());
    let dry = Physics.getNumber(args.shift());
    let isp = Physics.getNumber(args.shift());

    if (dry <= 0 || wet <= 0 || isp <= 0) {
        chatData.content = text + "Invalid values</div>";
        ChatMessage.create(chatData);
        return;
    }
    let ratio = wet / dry;
    let log = Math.log(ratio);
    let deltaVee = log * isp * Physics.g;

    text += `<b>${game.i18n.localize("PhysicsTools.WetMass")}</b>: ${wet.toLocaleString()}<br/>`;
    text += `<b>${game.i18n.localize("PhysicsTools.DryMass")}</b>: ${dry.toLocaleString()}<br/>`;
    text += `<b>${game.i18n.localize("PhysicsTools.MassRation")}</b>: ${Physics.printNumber(ratio)}<br/>`;
    text += `<b>${game.i18n.localize("PhysicsTools.Isp")}</b>: ${Physics.printNumber(isp)}<br/>`;
    if (deltaVee >= 10000) {
        text += `<b>${game.i18n.localize("PhysicsTools.DeltaVee")}</b>: ${Number((deltaVee / 1000.0).toPrecision(4)).toLocaleString()} kms<sup>-1</sup><br/>`;
    } else {
        text += `<b>${game.i18n.localize("PhysicsTools.DeltaVee")}</b>: ${Physics.printNumber(deltaVee)} ms<sup>-1</sup><br/>`;
    }
    text += `</div>`;

    Physics.chatMessage(text);
};

Physics.lagCommand = function(chatData, args) {
    let text = `<div class="physics-tools">`;
    text += `<h3>${game.i18n.localize("PhysicsTools.LightLag")}</h3>`;

    if (args.length < 1) {
        return;
    }
    let distance = Physics.getDistance(args.shift());
    let seconds = distance / 299792458;
    let time = Physics.printTime(seconds);

    text += `<b>${game.i18n.localize("PhysicsTools.Distance")}</b>: ${Physics.printDistance(distance)}<br/>`;
    text += `<b>${game.i18n.localize("PhysicsTools.TimeDelay")}</b>: ${Physics.printTime(seconds)}<br/>`;
    text += `<b>${game.i18n.localize("PhysicsTools.BothWays")}</b>: ${Physics.printTime(seconds * 2)}<br/>`;
    text += `</div>`;

    Physics.chatMessage(text);
}

