import { Physics } from "./physics.mjs";

Hooks.once('init', async function() {
    game.settings.register('physics-tools', 'useDiameter', {
        name: game.i18n.localize("PhysicsTools.UseDiameter.Name"),
        hint: game.i18n.localize("PhysicsTools.UseDiameter.Hint"),
        scope: 'client',
        config: true,
        type: Boolean,
        default: false
    }),
    game.settings.register('physics-tools', 'whisper', {
        name: game.i18n.localize("PhysicsTools.Whisper.Name"),
        hint: game.i18n.localize("PhysicsTools.Whisper.Hint"),
        scope: 'client',
        config: true,
        type: Boolean,
        default: true
    })
})

Hooks.on("chatMessage", function(chatlog, message, chatData) {
    if (message.indexOf("/phy") === 0) {
        let args = message.split(" ");
        args.shift()
        if (args.length === 0) {
            Physics.help(chatData);
            return false;
        } else if ("planet".startsWith(args[0])) {
            args.shift();
            Physics.planetCommand(chatData, args);
            return false;
        } else if ("thrust".startsWith(args[0])) {
            args.shift();
            Physics.thrustCommand(chatData, args);
            return false;
        } else if ("ethrust".startsWith(args[0])) {
            args.shift();
            Physics.eThrustCommand(chatData, args);
            return false;
        } else if ("rocket".startsWith(args[0])) {
            args.shift();
            Physics.rocketCommand(chatData, args);
            return false;
        } else if ("lag".startsWith(args[0])) {
            args.shift();
            Physics.lagCommand(chatData, args);
            return false;
        }
    }
});

